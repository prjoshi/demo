<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'title' => 'Admin',
                'description' => 'Admin'
            ],
            [
                'title' => 'Doctor',
                'description' => 'Doctor'
            ],
            [
                'title' => 'Pharmacist',
                'description' => 'Pharmacist'
            ],
            [
                'title' => 'Nurse',
                'description' => 'Nurse'
            ],
            [
                'title' => 'Register',
                'description' => 'Register'
            ],
            [
                'title' => 'Unauthorized',
                'description' => 'Unauthorized'
            ],
        ];
        foreach ($roles as $role) {
            Role::updateOrCreate(['title' => $role['title']], $role);
        }
    }
}
