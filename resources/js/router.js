import Vue from 'vue'
import VueRouter from 'vue-router'

import userCreate from "./components/users/create";
import userList from "./components/users/list";
import userEdit from "./components/users/edit";


Vue.use(VueRouter);

const router = [
    {path: '/', component: userList},
    {path: '/create-user', component: userCreate},
    {path: '/user-edit/:id', 'name': 'user-edit', component: userEdit}
]

export default new VueRouter({
    routes:router
});
