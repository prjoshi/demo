@extends('layouts.auth')

@section('content')
    <div class="login-form login-signin">
        <div class="text-center mb-10 mb-lg-20">
            <h3 class="font-size-h1">Sign In</h3>
            <p class="text-muted font-weight-bold">Enter your username and password</p>
        </div>
        <!--begin::Form-->
        <form action="{{ route('login') }}" method="post" class="form" novalidate="novalidate">
            @csrf
            <div class="form-group">

                <input id="email" type="email" class="form-control form-control-solid h-auto py-5 px-6 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input id="password" type="password" class="form-control form-control-solid h-auto py-5 px-6 @error('password') is-invalid @enderror" name="password" required placeholder="Password" autocomplete="current-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <!--begin::Action-->
            <div class="form-group d-flex flex-wrap justify-content-between align-items-center">
                @if (Route::has('password.request'))
                    <a class="text-dark-50 text-hover-primary my-3 mr-2" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif
                <button type="submit" id="kt_login_signin_submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3">Sign In</button>
            </div>
            <!--end::Action-->
        </form>
        <!--end::Form-->
    </div>
</div>
@endsection
